import { writable } from 'svelte/store'
import { fetchTodos } from './todos.js'

let initialState = {
	todos: {
		currentState: undefined,
		data: [],
		error: undefined
	}
}

let createStore = () => {
	
	let { subscribe, update } = writable(initialState)
	
  const mutateState = {
		todosLoading() {
			update(state => {
				state.todos.error = undefined
				state.todos.data = []
				state.todos.currentState = 'loading'
				return state
			})				
		},
		todosLoaded(data) {
			update(state => {
				state.todos.currentState = 'loaded'
				state.todos.data = data
				state.todos.error = undefined
				return state
			})						
		},
		todosError(err) {
			update(state => {
				state.todos.currentState = 'error'
				state.todos.data = []
				state.todos.error = err
				return state
			})
		},
		todosReset() {
			update(state => {
				state.todos.currentState = undefined
				state.todos.data = []
				state.todos.error = undefined
				return state
			})
		}
	}
	
	
	
	const action = {
			async loadTodos() {
				try {
					mutateState.todosLoading()
					const data = await fetchTodos()
					mutateState.todosLoaded(data)
				} catch(err) {
					mutateState.todosError(err)
				}
			},
		  resetTodos() {
				console.log('invoked resetTodos');
				mutateState.todosReset()
			}
	}

	return {
		subscribe,
		action
	}
}

const store = createStore()

export default store