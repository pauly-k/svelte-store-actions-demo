import { delay } from './util.js'

const todos = [
	{
		title: 'Design a JSON format to represent the flat fire payload structures ans rules'
	},
	{
		title: 'Write a function to convert flat files to human readable JSON'
	},
	{
		title: 'Write a component to diplay the converted flat file JSON to html, css'
	},
	{
		title: 'Persist the interface / method and its JSON rules along with onre or more mock data samples '
	},
	{
		title: 'Interface "method list"'
	},
	{
		title: 'clciking on a a list shows the title, JSON rules, and raw mock data'
	},
	{
		title: 'Click view opens a modal overlay show the results'
	},
	{
		title: 'convert raw flat text to human readable format'
	},
]

export const fetchTodos = async () => {
	await delay(500)	
	if (Math.random() > 0.1) {
		return todos
	}
	throw Error("Error loading todos")
}



