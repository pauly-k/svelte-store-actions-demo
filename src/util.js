export const delay = (ms = 1500) => 
  new Promise(res => setTimeout(res, ms))